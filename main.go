package main

import (
	"bufio"
	"fmt"
	"os"
	"os/exec"
	"strings"
)

func main() {
	reader := bufio.NewReader(os.Stdin)
	for {
		fmt.Print("> ")

		input, err := reader.ReadString('\n')
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
		}

		if err = execInput(input); err != nil {
			fmt.Fprintln(os.Stderr, err)
		}
	}
}

func execInput(input string) error {
	// remove the newline character
	input = strings.TrimSuffix(input, "\n")

	// split the input to seperate the command and the arguments
	args := strings.Split(input, " ")

	switch args[0] {
	case "cd":
		if len(args) == 1 {
			return os.Chdir(os.Getenv("HOME"))
		}

		return os.Chdir(args[1])
	case "exit":
		os.Exit(0)
	}

	// prepare the command to execute
	cmd := exec.Command(args[0], args[1:]...)

	// set the correct output device
	cmd.Stderr = os.Stderr
	cmd.Stdout = os.Stdout

	// execute the command and return the error
	return cmd.Run()
}
